Rails.application.routes.draw do
  devise_for :users
  root to: 'pages#index'
  resources :books
  resources :orders

  get "/about", to: "pages#about"
  get "/search", to: "pages#search"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
