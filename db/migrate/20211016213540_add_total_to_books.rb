class AddTotalToBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :books, :total, :integer, default: 0
    add_column :books, :top_seller, :boolean, default: false
  end
end
