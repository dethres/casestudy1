class CreateOrdersBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :orders_books do |t|
      t.belongs_to :order, null: false, foreign_key: true
      t.belongs_to :book, null: false, foreign_key: true

      t.timestamps
    end
  end
end
