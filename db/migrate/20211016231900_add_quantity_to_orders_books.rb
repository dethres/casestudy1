class AddQuantityToOrdersBooks < ActiveRecord::Migration[6.1]
  def change
    add_column :orders_books, :quantity, :integer
  end
end
