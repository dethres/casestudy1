class PagesController < ApplicationController
  def index
  end
  def about
  end
  #this on is not a static page sort of
  # but I find it useful to have it here instead of another controller
  def search
    pattern = params[:s]
    @books = Book.where("name LIKE ?","%#{pattern}%")
  end
end
