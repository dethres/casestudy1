class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order, only: %i[ show edit update destroy ]

  # GET /orders or /orders.json
  def index
    if current_user.admin_role? || current_user.superadmin_role?
      @orders = Order.all
    else
      @orders = Order.where("user_id = ?", current_user.id)
    end
  end
  
  # GET /orders/1 or /orders/1.json
  def show
    #get price, name, quantity
    @acum = 0
    @ticketing = Array.new
    @order.books.each do |book|
      #get quantity
      quantity = OrdersBook.where("order_id= ? AND book_id = ?", @order.id,book.id)
      @ticketing.push([book.name,book.price,quantity[0].quantity])
      @acum = @acum+book.price.to_i * quantity[0].quantity.to_i
    end
    #binding.pry
  end
  
  # GET /orders/new
  def new
    @order = Order.new
  end

  # GET /orders/1/edit
  def edit
  end

  # POST /orders or /orders.json
  def create
    @order = Order.new(order_params)
    @order.user = current_user

    respond_to do |format|
      if @order.save
        format.html { redirect_to @order, notice: "Order was successfully created." }
        format.json { render :show, status: :created, location: @order }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /orders/1 or /orders/1.json
  def update
    binding.pry
    if !params[:s].nil?
      if params[:s] == "cancel"
        if @order.may_cancel?
          @order.cancel
          flash.now[:notice]= "Order Canceled"
        else
          flash.now[:alert] = "Cannot cancel, Order is: #{@order.status}"
        end

      end
      if params[:s] == "finalize"
        if @order.may_delivered?
          @order.delivered
          flash.now[:notice]= "Order Finalized"
        else
          flash.now[:alert] = "Cannot cancel, Order is: #{@order.status}"
        end
      end
    end
    respond_to do |format|
      if @order.update(order_params)
        format.html { redirect_to @order, notice: "Order was successfully updated." }
        format.json { render :show, status: :ok, location: @order }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1 or /orders/1.json
  def destroy
    @order.destroy
    respond_to do |format|
      format.html { redirect_to orders_url, notice: "Order was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def order_params
      params.require(:order).permit(:user_id,:delivery_method,:payment_method,orders_books_attributes: [:id,:book_id,:quantity,:_destroy])
    end
end
