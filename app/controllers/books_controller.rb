class BooksController < ApplicationController
  before_action :authenticate_user!, except: %i[index]
  before_action :set_book, only: %i[ show edit update destroy ]

  # GET /books or /books.json
  def index
    @books = Book.all
    @top_seller = Book.where(top_seller: true).limit(5)
    @top_seller.shuffle
  end

  # GET /books/1 or /books/1.json
  def show
    ids = Array.new
    recommendedBooksObjectsID = BooksCategory.where("category_id = ?",@book.categories.first.id)
    recommendedBooksObjectsID.each do |book|
      if book.book_id ==  @book.id
        #asi no tenemos el mismo libro lmao
      else
        ids.push(book.book_id)
      end
    end
    @recommendedBooks = Book.where(id: ids[0...4])
  end

  # GET /books/new
  def new
    @book = Book.new
  end

  # GET /books/1/edit
  def edit
  end

  # POST /books or /books.json
  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        format.html { redirect_to @book, notice: "Book was successfully created." }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1 or /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to @book, notice: "Book was successfully updated." }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1 or /books/1.json
  def destroy
    @book.destroy
    respond_to do |format|
      format.html { redirect_to books_url, notice: "Book was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def book_params
      params.require(:book).permit(:name, :author, :number_of_copies,:total,:top_seller,:price,:cover,books_categories_attributes: [:id, :category_id,:_destroy])
    end
end
