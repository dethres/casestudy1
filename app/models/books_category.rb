# == Schema Information
#
# Table name: books_categories
#
#  id          :bigint           not null, primary key
#  category_id :bigint           not null
#  book_id     :bigint           not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class BooksCategory < ApplicationRecord
  belongs_to :category
  belongs_to :book
end
