# == Schema Information
#
# Table name: orders
#
#  id              :bigint           not null, primary key
#  payment_method  :string
#  delivery_method :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
class Order < ApplicationRecord
  include AASM
  belongs_to :user
  has_many :orders_books
  has_many :books, through: :orders_books
  accepts_nested_attributes_for :orders_books, reject_if: :all_blank, allow_destroy: true
  aasm column: :status do
    state :in_progress, initial: true
    state :finalized
    state :cancelled

    event :delivered do
      transitions from: :in_progress, to: :finalized
    end
    event :cancel do
      transitions from: :in_progress, to: :cancelled
    end
  end
  before_save do |order|
    puts order.status
  end
end
