# == Schema Information
#
# Table name: books
#
#  id               :bigint           not null, primary key
#  name             :string
#  author           :string
#  number_of_copies :integer
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Book < ApplicationRecord
  has_one_attached :cover
  has_many :books_categories
  has_many :categories, through: :books_categories
  has_many :orders, through: :orders_books
  has_many :orders_books
  accepts_nested_attributes_for :books_categories, reject_if: :all_blank, allow_destroy: true
end
