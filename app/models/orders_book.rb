# == Schema Information
#
# Table name: orders_books
#
#  id         :bigint           not null, primary key
#  order_id   :bigint           not null
#  book_id    :bigint           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class OrdersBook < ApplicationRecord
  belongs_to :order
  belongs_to :book
end
